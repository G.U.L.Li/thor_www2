#!/usr/bin/env python3

# Analisi dati THOR WW2 - Step 2
# utilizzo di pandas

import psycopg2
import json
import pandas

conn = psycopg2.connect(
    host="linux.livorno.it",
    database="postgres",
    user="gulli-thorww2",
    password="gullithorwwii")

# Ritorna i risultati dell'interrogazione in formato JSON

cur = conn.cursor()
cur.execute("SELECT row_to_json(row) FROM ( \
             SELECT * FROM thor_wwii_data_clean LIMIT 20 \
             ) AS row")
missions = cur.fetchall()

# print( json.dumps(missions, indent=4))
print("Sono stati trovati {} records".format(len(missions)))

# Carica i dati json in un data frame pandas

thordata = pandas.read_json(json.dumps(missions))
print(thordata)
