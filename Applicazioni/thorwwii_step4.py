#!/usr/bin/env python3

# Analisi dati THOR WW2 - Step 4 (geopandas)
# utilizzo di pandas

import psycopg2
import pandas as pd
import geopandas as gpd
import matplotlib.pyplot as plt

conn = psycopg2.connect(
    host="linux.livorno.it",
    database="postgres",
    user="gulli-thorww2",
    password="gullithorwwii")

# Ritorna i risultati dell'interrogazione in formato JSON

sql = "SELECT wwii_id,latitude,longitude FROM thor_wwii_data_clean \
       WHERE theater IN ('ETO', 'MTO') AND longitude<=180"
missions = pd.read_sql_query(sql, conn)
gdf = gpd.GeoDataFrame(missions, geometry=gpd.points_from_xy(
                                    missions.longitude, missions.latitude))

print("Trovate %d missioni." % gdf.wwii_id.size)
# print (gdf)

# Mappa mondiale delle missioni

fig, ax = plt.subplots()
fig.set_size_inches(20, 20)

# Limita mappa alle coordinate delle missioni
ax.set_aspect('equal')
# added/substracted value is to give some margin around total bounds
ax.set_xlim(gdf['longitude'].min() - .1,  gdf['longitude'].max() + .1)
ax.set_ylim(gdf['latitude'].min() - .1,  gdf['latitude'].max() + .1)

# Colore del mare
water = 'lightcyan'
ax.set_facecolor(water)

world = gpd.read_file(gpd.datasets.get_path('naturalearth_lowres'))
world.plot(ax=ax, color='gainsboro', edgecolor='grey')
# world.plot(ax=ax)

gdf.plot(ax=ax, color='red', markersize=1)

plt.show()
