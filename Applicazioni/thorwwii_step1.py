#!/usr/bin/env python3

# Esempio per collegamento al DB server postgres del GULLi

import psycopg2
import json

conn = psycopg2.connect(
    host="linux.livorno.it",
    database="postgres",
    user="gulli-thorww2",
    password="gullithorwwii")

# Ritorna i risultati dell'interrogazione in formato JSON

cur = conn.cursor()
cur.execute("SELECT row_to_json(row) FROM ( \
             SELECT * FROM thor_wwii_data_clean LIMIT 20 \
             ) AS row")
missions = cur.fetchall()

print(json.dumps(missions, indent=4))
print("Sono stati trovati {} records".format(len(missions)))
