#!/usr/bin/env python3

# Analisi dati THOR WW2 - Step 2 (alternativo, semplificato)
# utilizzo di pandas

import psycopg2
import pandas as pd

conn = psycopg2.connect(
    host="linux.livorno.it",
    database="postgres",
    user="gulli-thorww2",
    password="gullithorwwii")

# Ritorna i risultati dell'interrogazione in formato JSON

sql = "SELECT * FROM thor_wwii_data_clean LIMIT 20"
missions = pd.read_sql_query(sql, conn)
print(missions)
