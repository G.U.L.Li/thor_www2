# Creazione virtualenv ed installazione dipendenze
```sh
python3 -m virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
```

# THOR WWII Data Analysis project

Applicazioni e queries per l'analisi del dataset ["Theater History of Operations (THOR) Data: World War II"](https://data.world/datamil/world-war-ii-thor-data). Il dataset riporta i dati dei bombardamenti aerei degli alleati durante la seconda guerra mondiale e pensiamo che l’analisi possa creare diversi interessi sia storici (quando, dove e come sono avvenuti i bombardamenti) che tecnici (come analizzare i file CSV con Python, geopandas, SQL). 

The GULLi LUG (Gruppo Utenti Linux Livorno) project aims to analyze the [THOR WWI dataset](https://data.world/datamil/world-war-ii-thor-data), the dataset of the allied war missions during the World War II and to correlate historical events with the database records. The objective is to link data analysis to historical events published on the web. The repository contains applications and code snaphosts in support of specific data analysis and tecniques.

## Database
Il dataset è stato caricato su un DB server Postgres disponibile per interrogazioni di sola lettura.

The dataset has been loaded on a Postgres DB server free available for read-only queries.

## More info
Ulteriori informazioni sul progetto e sulle risorse disponibili sul [sito web del GULLi](https://linux.livorno.it/sito/2558/analisi-del-dataset-theater-history-of-operations-thor-data-world-war-ii/).

More info on the project and resources on the [GULLi web site](https://linux.livorno.it/sito/2558/analisi-del-dataset-theater-history-of-operations-thor-data-world-war-ii/).
