-- Primo ed ultimo bombardamento per ogni stato (solo teatri europeo e mediterraneo)
-- First and last bombing data for each country. The query is limited to the european and mediterranean operation teathers.
-- Author: Gruppo Utenti Linux Livorno - Analisi dataset THOR WW2
-- File: primo_ultimo_bombardamento_stato.sql


SELECT tgt_country, MIN(msndate), MAX(msndate), (MAX(msndate)-MIN(msndate))+1 as duration, theater
 FROM public.thor_wwii_data_clean
 WHERE theater IN ('ETO', 'MTO')
 group by tgt_country, theater
 order by MIN(msndate)
