-- Elenco dei bombardamenti con obiettivo Livorno (Leghorn)
-- Author: Gruppo Utenti Linux Livorno - Analisi dataset THOR WW2
-- File: livorno.sql

SELECT *
	FROM public.thor_wwii_data_clean
	where tgt_country ilike 'Italy'
	and tgt_location ilike 'leghorn%'
	order by msndate
